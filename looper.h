/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef LOOPER_H_
#define LOOPER_H_

#include <iostream>
#include <sstream>
#include <vector>
#include <ctime>
#include "wave_file.h"

#define EMPTY 0
#define RECORDING 1
#define PLAYING 2
#define STOPPED 3

#define ML_UNSET 0
#define ML_RECORDING 1
#define ML_SET 2

#define N_LOOPS 8

typedef struct
{
    unsigned int status;
    unsigned int pos;
    std::vector<float> loop;
} loop_sets_t;

extern loop_sets_t loops[N_LOOPS];

void init_looper();
void run_looper(const float &input, float &output);
void change_loop_status(const unsigned char &loop_number);
void clear_loop(const unsigned char &loop_number);
bool save_loops_to_files();

#endif /* LOOPER_H_ */
