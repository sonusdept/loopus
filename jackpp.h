/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef JACKPP_H_
#define JACKPP_H_

#include <iostream>
#include <vector>

#include <jack/jack.h>

int process(jack_nframes_t nframes, void *arg);

class JackPP
{
public:
    virtual ~JackPP()
    {
        jack_client_close(client);
    }

    static void jack_shutdown(void *arg)
    {
        std::cerr << "JACK server stopped\n";
    }

    virtual int audio_process(jack_nframes_t nframes)
    {
        return 0;
    }

    bool instantiate(const char *client_name, unsigned int in_channels,
                     unsigned int out_channels, const char *input_prefix = "input",
                     const char *output_prefix = "output");

    float get_sample_rate();

protected:
    std::vector<jack_port_t*> input_ports;
    std::vector<jack_port_t*> output_ports;
    jack_client_t *client;
    float sample_rate;
};

#endif // JACKPP_H_
