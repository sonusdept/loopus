/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef AUDIOIO_H_
#define AUDIOIO_H_

#include <cstring>
#include <thread>

#include "jackpp.h"
#include "looper.h"

class AudioIO: public JackPP
{
public:
    AudioIO();
    ~AudioIO();

    int audio_process(jack_nframes_t nframes);
};

extern AudioIO *audio_io;

#endif /* AUDIOIO_H_ */
