/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "audioio.h"

typedef jack_default_audio_sample_t sample_t;

void processing_thread(sample_t *in, sample_t *out, const jack_nframes_t &nframes)
{
    for (jack_nframes_t s = 0; s < nframes; s++)
    {
        run_looper(in[s], out[s]);
    }
}

AudioIO::AudioIO()
{

}

AudioIO::~AudioIO()
{

}

int AudioIO::audio_process(jack_nframes_t nframes)
{
    sample_t *out = (sample_t *)jack_port_get_buffer(output_ports[0], nframes);
    sample_t *in = (sample_t *)jack_port_get_buffer(input_ports[0], nframes);
    
    std::thread processing(processing_thread, in, out, nframes);

    processing.join();
    // processing_thread(in, out, nframes);

    return 0;
}
