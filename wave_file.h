/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef WAVE_FILE_H_
#define WAVE_FILE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <stdint.h>
#include <cmath>
#include <vector>

#define PCM_WAVE 0
#define RAW 1

typedef struct
{
    std::string name_;
    std::fstream data_;
    unsigned int size_;
    unsigned int sample_rate_;
    unsigned int bit_depth_;
    unsigned int channels_;
} wave_file_sets_t;


template <class fp_sample_t>
class WaveFileIn
{
public:
    WaveFileIn()
    {

    }

    ~WaveFileIn()
    {
        if (file_.data_.is_open())
            file_.data_.close();
    }

    bool open(char *file_name, int format = PCM_WAVE, fp_sample_t sample_rate = 44100.0, unsigned int bit_depth = 16, unsigned int channels = 1)
    {
        file_.data_.open(file_name, std::ios::in | std::ios::binary);

        if (!file_.data_.is_open())
        {
            std::cerr << "Unable to open " << file_name << "\n";

            return false;
        }

        switch (format)
        {
        case PCM_WAVE:
            if (!parse_pcm_wave_header())
            {
                std::cerr << file_name << " is not a valid WAVE file or is of an unsupported format\n";

                return false;
            }
            break;
        case RAW:
            file_.sample_rate_ = sample_rate;
            file_.bit_depth_ = bit_depth;
            file_.channels_ = channels;
            break;
        }

        file_.name_ = file_name;

        switch (file_.bit_depth_)
        {
        case 8:
            float_conv_const_ = 1.0 / pow(2, 7);
            float_add_const_ =  -1.0;
            break;
        case 16:
            float_conv_const_ = 1.0 / pow(2, 15);
            float_add_const_ = 0.0;
            break;
        default:
            std::cerr << file_.bit_depth_ << " bit files are not supported yet\n";
            return false;
        }


        return true;
    }

    bool close()
    {
        file_.data_.close();

        return true;
    }

    void print_info()
    {
        std::cout << "Sample rate:\t" << file_.sample_rate_ << "\n";
        std::cout << "Bit depth:\t" << file_.bit_depth_ << "\n";
        std::cout << "Channels:\t" << file_.channels_ << "\n";
    }

    bool eof()
    {
        return end_of_file_;
    }

    inline fp_sample_t fread()
    {
        fp_sample_t fsample;

        return fsample = ((fp_sample_t)read() * float_conv_const_) + float_add_const_;
    }

    inline int read()
    {
        if (file_.data_.good())
        {
            end_of_file_ = false;
            switch (file_.bit_depth_)
            {
            case 8:
                file_.data_.read(sample_8_, 2);
                return (int)(*(uint8_t*)sample_8_);
            case 16:
                file_.data_.read(sample_16_, 4);
                return (int)(*(int16_t*)sample_16_);
            default:
                std::cerr << "Not supported\n";
            }
        }

        end_of_file_ = true;
        return 0;
    }

    unsigned int get_channels()
    {
        return file_.channels_;
    }

    unsigned int get_sample_rate()
    {
        return file_.sample_rate_;
    }

    unsigned int get_bit_depth()
    {
        return file_.bit_depth_;
    }


private:
    wave_file_sets_t file_;
    char sample_8_[2];
    char sample_16_[4];
    //char sample_24_[6];
    fp_sample_t float_conv_const_;
    fp_sample_t float_add_const_;
    bool end_of_file_;

    bool parse_pcm_wave_header()
    {
        char current_offset[4];
        char current_offset_short[2];

        // Read ChunkID, which has to be 'RIFF'
        file_.data_.read(current_offset, 4);
        if (strcmp(current_offset, "RIFF"))
            return false;

        // Read file size, and store the value
        file_.data_.read(current_offset, 4);
        file_.size_ = *((uint32_t*)current_offset);

        // Read format, which has to be 'WAVE'
        file_.data_.read(current_offset, 4);
        if (strcmp(current_offset, "WAVE"))
            return false;

        // Read SubChunk1ID, which has to be 'fmt '
        file_.data_.read(current_offset, 4);
        if (strcmp(current_offset, "fmt "))
            return false;

        // Read SubChunk1Size, which has to be 16 on its first block
        file_.data_.read(current_offset, 4);
        if (*((uint32_t*)current_offset) != 16)
            return false;

        // Read AudioFormat, which has to be 1 on its first block
        file_.data_.read(current_offset_short, 2);
        if (*((uint16_t*)current_offset_short) != 1)
            return false;

        // Read NumChannels, and store the value
        file_.data_.read(current_offset_short, 2);
        file_.channels_ = *((uint16_t*)current_offset_short);

        // Read SampleRate, and store the value
        file_.data_.read(current_offset, 4);
        file_.sample_rate_ = *((uint32_t*)current_offset);

        // Read ByteRate (SampleRate * NumChannels * BitsPerSample / 8)
        file_.data_.read(current_offset, 4);

        // Read BlockAlign (NumChannels * BitsPerSample / 8)
        file_.data_.read(current_offset_short, 2);

        // Read BitsPerSample, and store the value
        file_.data_.read(current_offset_short, 2);
        file_.bit_depth_ = *((uint16_t*)current_offset_short);

        // Read SubChunk2ID, which has to be 'data'
        file_.data_.read(current_offset, 4);
        if (strcmp(current_offset, "data"))
            return false;

        // Read SubChunk2Size (NumSamples * NumChannels * BitsPerSample / 8)
        file_.data_.read(current_offset, 4);

        return true;
    }
};

template <class fp_sample_t>
class WaveFileOut
{
public:
    WaveFileOut()
    {

    }
    ~WaveFileOut()
    {
        if (file_.data_.is_open())
            close();
    }

    bool create(const char *file_name, int format = PCM_WAVE, unsigned int sample_rate = 44100, unsigned int bit_depth = 16, unsigned int channels = 1)
    {
        file_.data_.open(file_name, std::ios::out | std::ios::binary);

        if (!file_.data_.is_open())
        {
            std::cerr << "Unable to open " << file_name << "\n";

            return false;
        }

        format_ = format;

        if (channels > 0)
            file_.channels_ = channels;
        else
        {
            std::cerr << "Channels cannot be zero\n";
            return false;
        }

        if (sample_rate > 0)
            file_.sample_rate_ = sample_rate;
        else
        {
            std::cerr << "Sample rate cannot be zero\n";
            return false;
        }

        if (bit_depth == 8 || bit_depth == 16)
            file_.bit_depth_ = bit_depth;
        else
        {
            std::cerr << bit_depth << " bit files are not supported yet\n";
            return false;
        }

        file_.name_ = file_name;

        return true;
    }

    bool close()
    {
        if (pull_data())
        {
            file_.data_.close();
            if (!file_.data_.is_open())
                return true;
        }

        return false;
    }

    bool pull_data()
    {
        bool status = true;
        file_.size_ = 36 + ((samples_.size() * file_.channels_ * file_.bit_depth_) / 8);

        if (format_ == PCM_WAVE)
            status = write_header();

        switch (file_.bit_depth_)
        {
        case 8:
        {
            uint8_t sample_8_ = 0;
            for (unsigned int s = 0; s < samples_.size(); s++)
            {
                if (samples_.at(s) > 1.0)
                    samples_.at(s) = 1.0;
                if (samples_.at(s) < -1.0)
                    samples_.at(s) = -1.0;

                sample_8_ = (uint8_t)(((samples_.at(s) + 1.0) * 0.5) * 255.0);
                file_.data_.write((char*)&sample_8_, 2);
            }
            break;
        }
        case 16:
        {
            int16_t sample_16_ = 0;
            for (unsigned int s = 0; s < samples_.size(); s++)
            {
                if (samples_.at(s) > 1.0)
                    samples_.at(s) = 1.0;
                if (samples_.at(s) < -1.0)
                    samples_.at(s) = -1.0;

                sample_16_ = (int16_t)(samples_.at(s) * 32767.0);
                file_.data_.write((char*)&sample_16_, 4);
            }
            break;
        }
        }
        return status;

    }

    bool fwrite(const fp_sample_t &fsample)
    {
        samples_.push_back(fsample);

        return true;
    }
    bool write(const int &sample)
    {
        /*
           switch (file_.bit_depth_)
           {
           case 8:
               uint8_t sample_8_ = (uint8_t)(((samples_.at(s) * 0.5) + 1.0) * 255.0);
               file_.data_.write(sample_8_, 2);
               break;
           case 16:
               int16_t sample_16_ = (int16_t)(samples_.at(s) * 32767.0);
               file_.data_.write(sample_16_, 4);
               break;
           }*/

        return true;
    }

private:
    wave_file_sets_t file_;
    std::vector<fp_sample_t> samples_;
    int format_;
    bool write_header()
    {
        // Write ChunkID, which has to be 'RIFF'
        file_.data_.write("RIFF", 4);

        // Write file size
        file_.data_.write((char*)&file_.size_, 4);

        // Write format, which is 'WAVE'
        file_.data_.write("WAVE", 4);

        // Write SubChunk1ID, which is 'fmt '
        file_.data_.write("fmt ", 4);

        // Write SubChunk1Size, which is 16 on its first block
        uint32_t sixteen = 16;
        file_.data_.write((char*)&sixteen, 4);

        // Write AudioFormat, which is 1 on its first block
        uint16_t one = 1;
        file_.data_.write((char*)&one, 2);

        // Write NumChannels, and store the value
        file_.data_.write((char*)&file_.channels_, 2);

        // Write SampleRate, and store the value
        file_.data_.write((char*)&file_.sample_rate_, 4);

        // Write ByteRate (SampleRate * NumChannels * BitsPerSample / 8)
        uint32_t byte_rate = (file_.sample_rate_ * file_.channels_ * file_.bit_depth_) / 8;
        file_.data_.write((char*)&byte_rate, 4);

        // Write BlockAlign (NumChannels * BitsPerSample / 8)
        uint16_t block_align = (file_.channels_ * file_.bit_depth_) / 8;
        file_.data_.write((char*)&block_align, 2);

        // Write BitsPerSample, and store the value
        file_.data_.write((char*)&file_.bit_depth_, 2);

        // Write SubChunk2ID, which has to be 'data'
        file_.data_.write("data", 4);

        // Write SubChunk2Size (NumSamples * NumChannels * BitsPerSample / 8)
        uint32_t sub_chunk_2_size = (samples_.size() * file_.channels_ * file_.bit_depth_) / 8;
        file_.data_.write((char*)&sub_chunk_2_size, 4);

        return true;
    }
};

#endif // WAVE_FILE_H_
