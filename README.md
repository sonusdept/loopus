Loopus - a MIDI controlled multitrack loop station

Loopus is a looper software that you can control with MIDI messages.
It has no GUI and runs on GNU/Linux (but porting to other operating systems
is very easy). It is thought to be used on low-end computers or in small PCs
(like the Raspberry Pi). Obviously no one prevents you to use it on modern, big
and incredibly fast computers.

The features that differentiates Loopus from similar (and often better)
softwares are:
- Completely MIDI controllable, you do not even need a monitor to use it
- Unlimited number of concurrent loops. At the time limited to 128 (which are
a lot) to fit one MIDI channel, but it can easily changed to have as many loops
as your RAM allows
- No overdubs: every add to the first ('master') loop is done by creating a new,
synchronized, loop, so you can later turn on and off every addition in every
moment
- All the loops (unless explicitly requested with a command line argument) are
automatically saved in separate .wav files, so that you can work on them later
in you favourite DAW or load in your hardware sampler

This is free software, licensed with GPL-3 license.

This software is developed by Valerio Orlandini, owner of Sonus Dept.
<http://www.sonusdept.com>
