CXX = g++
LIBS = -ljack -lasound -lpthread -lrt
MTFLAGS = -std=c++11 -pthread
DEFS = -D__UNIX_JACK__ -D__LINUX_ALSA__
CXXFLAGS = -O3 -Wextra
SRC = main.cpp looper.cpp RtMidi.cpp jackpp.cpp audioio.cpp
BIN = loopus

all:
	$(CXX) $(LIBS) $(MTFLAGS) $(DEFS) $(CXXFLAGS) $(SRC) -o $(BIN)

clean:
	rm -f $(BIN)
