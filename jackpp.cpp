/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include <cstdio>

#include "jackpp.h"

int process(jack_nframes_t nframes, void *arg)
{
    return static_cast<JackPP*>(arg)->audio_process(nframes);
}

bool JackPP::instantiate(const char *client_name, unsigned int in_channels,
                         unsigned int out_channels, const char *input_prefix,
                         const char *output_prefix)
{
    //const char **ports;
    const char *server_name = NULL;
    jack_options_t options = JackNullOption;
    jack_status_t status;

    client = jack_client_open(client_name, options, &status, server_name);
    if (client == NULL)
    {
        std::cerr << "jack_client_open() failed, status = " << status << "\n";
        if (status & JackServerFailed)
        {
            std::cerr << "Unable to connect to JACK server\n";
        }
        return false;
    }
    if (status & JackServerStarted)
    {
        std::cerr << "JACK server started\n";
    }
    if (status & JackNameNotUnique)
    {
        client_name = jack_get_client_name(client);
        std::cerr << "Unique name " << client_name << " assigned\n";
    }

    jack_set_process_callback(client, process, this);

    jack_on_shutdown(client, jack_shutdown, 0);

    for (unsigned int in = 0; in < in_channels; in++)
    {
        std::string port_name = input_prefix;
        if (in_channels > 1)
        {
            if (in_channels == 2)
                in == 0 ? port_name += " L" : port_name += " R";
            else
            {
                char port_n[4];
                sprintf(port_n, " %d", in);
                port_name += port_n;
            }
        }
        jack_port_t *input_port = jack_port_register(client, port_name.c_str(),
                                  JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, in);
        if (input_port == NULL)
        {
            std::cerr << "No more JACK ports available\n";
            return false;
        }
        input_ports.push_back(input_port);
    }

    for (unsigned int out = 0; out < out_channels; out++)
    {
        std::string port_name = output_prefix;
        if (out_channels > 1)
        {
            if (out_channels == 2)
                out == 0 ? port_name += " L" : port_name += " R";
            else
            {
                char port_n[4];
                sprintf(port_n, " %d", out);
                port_name += port_n;
            }
        }
        jack_port_t *output_port = jack_port_register(client, port_name.c_str(),
                                   JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, out);
        if (output_port == NULL)
        {
            std::cerr << "No more JACK ports available\n";
            return false;
        }
        output_ports.push_back(output_port);
    }

    if (jack_activate(client))
    {
        std::cerr << "Cannot activate client\n";
        return false;
    }

    sample_rate = (float)jack_get_sample_rate(client);

    return true;
}

float JackPP::get_sample_rate()
{
    return sample_rate;
}
