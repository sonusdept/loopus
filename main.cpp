/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include <iostream>
#include <cstring>

#include "audioio.h"
#include "RtMidi.h"

AudioIO *audio_io;
RtMidiIn *midi_in;
unsigned char midi_ch;
bool save_files_on_exit;

void midi_input(double deltatime, std::vector<unsigned char> *message, void *userData)
{
    if (message->at(0) == (143 + midi_ch) && message->at(2) > 0)
    {
        if (message->at(1) < N_LOOPS)
        {
        change_loop_status(message->at(1));
        }
    }

    if (message->at(0) == (175 + midi_ch) && message->at(1) == 86 && message->at(2) < 127)
    {
        save_loops_to_files();
    }

    if (message->at(0) == (175 + midi_ch) && message->at(1) == 86 && message->at(2) == 127)
    {
        if (save_files_on_exit)
        {
            save_loops_to_files();
        }

        exit(0);
    }

    if (message->at(0) == (175 + midi_ch) && message->at(1) == 87 && message->at(2) == 127)
    {
    	init_looper();
    }
    
    if (message->at(0) == (175 + midi_ch) && message->at(1) == 88)
    {
        if (message->at(2) < N_LOOPS)
        {
        clear_loop(message->at(1));
        }
    }
}

void usage()
{
    std::cout << "Usage:\n\n";
    std::cout << "Make sure that this software: receives MIDI input from a ";
    std::cout << "keyboard or any other device able to transmit MIDI data, ";
    std::cout << "receives audio in from the sound source you want ";
    std::cout << "to loop and sends audio out to the proper JACK client.\n";
    std::cout << "Press a key of your MIDI device to record the master loop, ";
    std::cout << "press it again when you are done. Then, the next loops you ";
    std::cout << "will record will be synchronized to the master loop.\n";
    std::cout << "To stop playing, press the corresponding key again; ";
    std::cout << "to resume playing, press the key once more and so on.\n\n";
    std::cout << "To save all the loops you made, just exit gracefully from ";
    std::cout << "the software (press any key + enter) or send the value 127 ";
    std::cout << "from CC 86.\n";
    std::cout << "To clear all the loops you recorded (including the master loop ";
    std::cout << "with its length), send the value 127 from CC 87.\n";
    std::cout << "To clear one loop, send the value corresponding to loop ";
    std::cout << "number from CC 88.\n";
    std::cout << "To disable loop file savings on exit, launch Looper with ";
    std::cout << "-ns option. You can still save the files with MIDI CC 86 (any ";
    std::cout << "value except 127).\n";
    std::cout << "To use a MIDI channel different than 1, -ch [int, 1-16]\n\n";
}

int main(int argc, char **argv)
{
    std::cout << "LOOPUS 1.0 | a MIDI controlled multitrack loop station\n\n";
    std::cout << "Copyright Valerio Orlandini  2015 :: source code is licensed under GPL license version 3\n";
    std::cout << "http://www.sonusdept.com\n\n";

    midi_ch = 1;
    save_files_on_exit = true;

    if (argc > 1)
    {
    	int opt = 1;
    	while (opt < argc)
    	{
    		if (!strcmp(argv[opt], "-h") || !strcmp(argv[opt], "--help"))
    		{
    			usage();
    			return 0;
    		}

    		if (!strcmp(argv[opt], "-ns"))
    		{
    			++opt;
    			save_files_on_exit = false;
    			continue;
    		}

    		if (!strcmp(argv[opt], "-ch"))
    		{
    			++opt;

    			if (argc > opt)
    			{
    				if (atoi(argv[opt]) >= 1 && atoi(argv[opt]) <= 16)
    				{
    					midi_ch = atoi(argv[opt]);
    					continue;
    				}
    				else
    				{
    					std::cerr << "Invalid MIDI channel (must be inside [1,16] range). Set to 1.\n";
    					continue;
    				}
    			}
    			else
    			{
    				std::cerr << "You must specify a MIDI channel. Set to 1.\n";
    				continue;
    			}
    		}

    	}
    }

    audio_io = new AudioIO();

    if (!audio_io->instantiate("Loopus", 1, 1, "Input", "Output"))
    {
        return -1;
    }

    RtMidi::Api api = RtMidi::UNIX_JACK;

    midi_in = new RtMidiIn(api, "Loopus");
    midi_in->openVirtualPort("MIDI in");
    midi_in->setCallback(&midi_input);

    init_looper();

    char in;
    std::cout << "Press any key + enter to exit...\n";
    std::cin >> in;

    if (save_files_on_exit)
    	save_loops_to_files();

    return 0;
}
