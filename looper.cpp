/*************************************************************************
   Loopus - a MIDI controlled multitrack loop station
   Copyright (C) 2015  Valerio Orlandini
    
    This file is part of Loopus.

    Loopus is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Loopus is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Loopus.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "looper.h"

loop_sets_t loops[N_LOOPS];
unsigned int master_loop;
unsigned int master_loop_status;
unsigned int master_loop_length;
unsigned int master_loop_sample;
bool recording;

void init_looper()
{
    for (unsigned int i = 0; i < N_LOOPS; i++)
    {
        loops[i].status = EMPTY;
        loops[i].loop.clear();
        loops[i].pos = 0;
    }
    
    recording = false;

    /* Master loop set to N_LOOPS until the first loop is recorded */
    master_loop = N_LOOPS;
    master_loop_status = ML_UNSET;
}

void run_looper(const float &input, float &output)
{
    output = 0.0;
    recording = false;
    
    if (master_loop_status == ML_RECORDING)
    {
        loops[master_loop].loop.push_back(input);
        output = input;
        return;
    }

    for (unsigned int i = 0; i < N_LOOPS; i++)
    {
        switch (loops[i].status)
        {
        case EMPTY:
            break;
        case RECORDING:
            if (master_loop == N_LOOPS)
            {
                master_loop = i;
                master_loop_status = ML_RECORDING;
                std::cout << "Recording master loop...\n";
            }
            if (master_loop == i)
            {
                loops[i].loop.push_back(input);
                output = input;
                return;
            }
            
            loops[i].loop.at(master_loop_sample) += input;
            output += loops[i].loop.at(master_loop_sample);
            recording = true;
            break;
        case PLAYING:
            output += loops[i].loop.at(master_loop_sample);
            break;
        case STOPPED:
            break;
        }
    }
    
    if (master_loop_status == ML_SET)
    {
        master_loop_sample >= master_loop_length - 1 ? master_loop_sample = 0 : ++master_loop_sample;
    }
    
    if (!recording)
    {
        output += input;
    }
}

void change_loop_status(const unsigned char &loop_number)
{
    if (loop_number >= N_LOOPS)
    {
        std::cerr << "Invalid loop number: " << (unsigned int)loop_number << "\n";
        return;
    }

    switch (loops[loop_number].status)
    {
    case EMPTY:
        loops[loop_number].status = RECORDING;
        std::cout << "Loop " << (unsigned int)loop_number << " recording...\n";
        break;
    case RECORDING:
        if (loop_number == master_loop)
        {
            master_loop_length = loops[loop_number].loop.size();
            master_loop_sample = 0;

            for (unsigned int l = 0; l < N_LOOPS; l++)
            {
                if (l != master_loop)
                {
                    loops[l].loop.assign(master_loop_length, 0.0);
                }
            }
            
            master_loop_status = ML_SET;
            std::cout << "Master loop recorded.\n";
        }
        loops[loop_number].status = PLAYING;
        std::cout << "Loop " << (unsigned int)loop_number << " playing...\n";
        break;
    case PLAYING:
        loops[loop_number].status = STOPPED;
        std::cout << "Loop " << (unsigned int)loop_number << " stopped.\n";
        break;
    case STOPPED:
        loops[loop_number].status = PLAYING;
        std::cout << "Loop " << (unsigned int)loop_number << " playing...\n";
        break;
    }
}

bool save_loops_to_files()
{
    time_t now = time(0);
    struct tm *date = localtime(&now);
    unsigned int saved_loop = 1;

    std::stringstream save_date;
    save_date << "_" << date->tm_hour << date->tm_min << "_" << date->tm_mday << "-" << date->tm_mon + 1 << "-" << date->tm_year - 100 << ".wav";

    for (unsigned int i = 0; i < N_LOOPS; i++)
    {
        std::cerr << "Working on loop " << i << "\n";
        if (loops[i].status != EMPTY)
        {
            std::cerr << "Saving loop " << i << "...\n";
            WaveFileOut<float> file_out;
            std::string file_name = "loop_";
            char loop_seq_number[4], loop_midi_number[4];
            sprintf(loop_seq_number, "%d", saved_loop++);
            sprintf(loop_midi_number, "%d", i);
            file_name += loop_seq_number;
            file_name += "_[midi ";
            file_name += loop_midi_number;
            file_name += "]_";
            file_name += save_date.str();
            file_out.create(file_name.c_str());
            for (unsigned int s = 0; s < loops[i].loop.size(); s++)
            {
                file_out.fwrite(loops[i].loop.at(s));
            }
            file_out.close();
            std::cerr << "Loop " << i << " saved correctly\n";
        }
    }

    return true;
}

void clear_loop(const unsigned char &loop_number)
{
    if (master_loop_status == ML_SET && (unsigned int)loop_number < N_LOOPS)
    {
        loops[(unsigned int)loop_number].loop.assign(master_loop_length, 0.0);
        return;
    }
    
    if (loop_number >= N_LOOPS)
    {
        std::cerr << "Invalid loop number: " << (unsigned int)loop_number << "\n";
    }
}
